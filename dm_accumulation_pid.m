function D = dm_accumulation_pid(alpha, PP, PPcontrol, Dprev)
% Use:
%        D = dm_accumulation_pid(alpha, PP, PPcontrol, Dprev)
%
% ATTENTION: only for two classes decision making problem
% This accumulation framework is based on an exponential accumulation
% framework, but it incorporates a corrective factor derived from a
% proportional-derivative analysis on a second set of probability 
% (PPcontrol):
%
% D(t)  = D(t-1)*alpha + (PP(t) - CF(t))*(1-alpha)
% where CF is the corrective factor:
% CF(t) = (PP(t) - 0.5) * PPcontrol(t) * ...
%                                 (1 - abs(PPcontrol(t) - PPcontrol(t-1)))
% 
% Input:
%   alpha     = incremental factor
%   PP        = it is a vector containing the output posterior
%               probabilities of a two-way classifier (one complement to 
%               the other) at time t
%   PPcontrol = it is a matrix (size = 2x2) containing the output posterior
%               probabilities of a two-way classifier in first row at time 
%               t and in the second at t-1 (one column complement to 
%               the other); first column has to contain the probabilities
%               of the leading class (i.e.: if PPcontrol(2,1) equals one
%               control is off, CF = 0, instead if PPcontrol(2,1) equals 
%               zero control is max, CF = PP)
%   Dprev     = Dprev consists in the D(t-1) value
%
% Output:
%   D         = value of the accumulation framework
    
    if nargin < 4
        error('Not enough input variables')
    end
        
    % D start value
    D = Dprev;
    
    % Corrective factor
    CF = (PP - 0.5) .* (1 - PPcontrol(2, 1)) .* ...
        (1 - abs(PPcontrol(2, 1) - PPcontrol(1, 1)));
    
    D = D * alpha + (PP - CF) * (1 - alpha);
    
% end function 
end
    
